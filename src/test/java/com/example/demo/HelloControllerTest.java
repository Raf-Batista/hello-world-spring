package com.example.demo;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.test.web.servlet.MockMvc;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.patch;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WebMvcTest(HelloController.class)

public class HelloControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @BeforeEach
    public void setUp() {
        HelloController.setUpUsers();
    }


    // GET multiple numbers with operator
    @Test
    void addNumbers_threeArgs_returnsSeven() throws Exception {
        mockMvc.perform(get("/add/2/+/5"))
                .andExpect(status().isOk())
                .andExpect(content().string("7"));
    }

    // GET count vowels
    @Test
    void countVowels_oneArg_returnsTen() throws Exception {
        mockMvc.perform(get("/count?string=these+grapes+are+awesome"))
                .andExpect(status().isOk())
                .andExpect(content().string("10"));
    }

    // POST
    @Test
    void replaceWord_twoArgs_returnsStringWithWordsReplaced() throws Exception {
        mockMvc.perform(post("/replace").param("string", "the bus went super fast around the driveway").param("first", "the").param("second", "silly"))
                .andExpect(content().string("silly bus went super fast around silly driveway"));
    }

    @Test
    void sayHello_noArgs_returnsHelloWorld() throws Exception {
        mockMvc.perform(get("/hello"))
                .andExpect(status().isOk())
                .andExpect(content().string("Hello World"));
    }

    @Test
    void sayHello_withName_returnsHelloTest() throws Exception {
        mockMvc.perform(get("/hello?name=test"))
                .andExpect(status().isOk())
                .andExpect(content().string("Hello test"));
    }

    @Test
    void sayHello_withFirstNameAndLastName_returnsHelloTestName() throws Exception {
        mockMvc.perform(get("/hello2?first=test&last=name"))
                .andExpect(status().isOk())
                .andExpect(content().string("Hello test name"));
    }

    @Test
    void return_path_variable() throws Exception {
        mockMvc.perform(get("/no-query/123"))
                .andExpect(status().isOk())
                .andExpect(content().string("The ID is 123"));
    }

    @Test
    void return_param_variable() throws Exception {
        mockMvc.perform(get("/no-query?id=123"))
                .andExpect(status().isOk())
                .andExpect(content().string("The ID is 123"));
    }

    @Test
    void return_using_value() throws Exception {
        mockMvc.perform(get("/return"))
                .andExpect(status().isOk())
                .andExpect(content().string("Should you return? yes"));
    }

    @Test
    void return_using_value2() throws Exception {
        mockMvc.perform(get("/return/?return=no"))
                .andExpect(status().isOk())
                .andExpect(content().string("Should you return? no"));
    }

    @Test
    void index_example() throws Exception {
        mockMvc.perform(get("/users"))
                .andExpect(content().json("[\"Test 1\",\"Test 2\",\"Test 3\",\"Test 4\",\"Test 5\"]"));
    }

    @Test
    void show_example() throws Exception {
        mockMvc.perform(get("/users/2"))
                .andExpect(content().string("Test 2"));
    }

    @Test
    void create_example() throws Exception {
        mockMvc.perform(post("/users").param("user", "I was created"))
                .andExpect(content().json("[\"Test 1\",\"Test 2\",\"Test 3\",\"Test 4\",\"Test 5\",\"I was created\"]"));

    }

    @Test
    void patch_example() throws Exception {
        mockMvc.perform(patch("/users/1").param("name", "Test One"))
                .andExpect(content().json("[\"Test One\",\"Test 2\",\"Test 3\",\"Test 4\",\"Test 5\"]"));
    }

    @Test
    void delete_example() throws Exception {
        mockMvc.perform(delete("/users/5"))
                .andExpect(content().json("[\"Test 1\",\"Test 2\",\"Test 3\",\"Test 4\"]"));
    }

}
