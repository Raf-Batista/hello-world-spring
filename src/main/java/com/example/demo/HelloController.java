package com.example.demo;

import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

@RestController
public class HelloController {
    public static ArrayList<String> users = new ArrayList<>();

    public static void setUpUsers() {
        ArrayList<String> newUsers = new ArrayList<>();
        for (int i = 0; i < 5; i++) {
            newUsers.add(String.format("Test %d", i+1));
        }

        users = newUsers;
    }

    // GET multiple numbers with operator
    @GetMapping("/add/{first}/{operator}/{second}")
    public int addNumbers(@PathVariable int first, @PathVariable int second, @PathVariable String operator) {
        int total = 0;

        if (operator.equals("+")) {
            total += first + second;
        }

        return total;
    }

    // GET count vowels

    @GetMapping("/count")
    public int countVowels(@RequestParam String string) {

        int total = 0;

        for (int i = 0; i < string.length(); i++) {
            char character = string.charAt(i);
            if (character == 'a' || character == 'e' || character == 'i' || character == 'o' || character == 'u') {
               total += 1;
            }
        }

        return total;
    }

    // POST replace words

    @RequestMapping(value = "/replace", method = RequestMethod.POST)
    public String replaceWord(@RequestParam String string, @RequestParam String first, @RequestParam String second) {
       return string.replaceAll(first, second);
    }


    // Single Param
    @GetMapping("/hello")
    public String say_hello(@RequestParam(value = "name", defaultValue = "World", required = false) String name) {
        return String.format("Hello %s", name);
    }

    // Multiple Params
    @GetMapping("/hello2")
    public String say_hello_full_name(@RequestParam(defaultValue = "wonderful", required = false) String first
                          , @RequestParam(defaultValue = "world", required = false) String last) {
        return String.format("Hello %s %s", first, last);
    }

    // Variable from URI, not Query String using Path Variable
    @GetMapping("/no-query/{id}")
    public String return_path_variable(@PathVariable String id) {
        return String.format("The ID is %s", id);
    }

    // Same example as above but using query string
    @GetMapping("/no-query")
    public String return_param_variable(@RequestParam String id) {
        return String.format("The ID is %s", id);
    }

    // Using value
    @GetMapping("/return")
    public String return_using_value(@RequestParam(value = "return", defaultValue = "yes", required = false) String shouldReturn ) {
        return String.format("Should you return? %s", shouldReturn);
    }

    /* REST EXAMPLES */

    // Index
    @RequestMapping(value = "/users", method = RequestMethod.GET)
    public ArrayList<String> getAllUsers() {
        return users;
    }

    // Show
    @RequestMapping(value = "/users/{id}", method = RequestMethod.GET)
    public String getUser(@PathVariable String id) {
        int userId;
        if (id != null) {
            userId = Integer.parseInt(id);
        } else {
            return "User not found";
        }

        return users.get(userId-1);
    }

    // Create
    @RequestMapping(value = "/users", method = RequestMethod.POST)
    public ArrayList<String> createNewUsers(@RequestParam String user) {
        users.add(user);
        return users;
    }

    // Patch
    @RequestMapping(value = "/users/{id}", method = RequestMethod.PATCH)
    public ArrayList<String> editUser(@RequestParam String name, @PathVariable int id) {
        users.set(id-1, name);
        return users;
    }

    // Delete
    @RequestMapping(value = "/users/{id}", method = RequestMethod.DELETE)
    public ArrayList<String> editUser(@PathVariable int id) {
        users.remove(id-1);
        return users;
    }

}
